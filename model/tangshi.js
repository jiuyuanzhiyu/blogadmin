const query = require('../db/query')

module.exports = {
  async PoetriesCount(ctx) {
    const res = await query(`SELECT poets.name, COUNT(poetries.id) AS poetries_count FROM poetries LEFT JOIN poets ON poets.id = poetries.poet_id GROUP BY poets.id ORDER BY poetries_count DESC LIMIT 10`)

    console.log(res)
    ctx.body = {
      code: 0,
      data: res
    }
  },

  async SearchPoet(ctx) {
    
  }
}